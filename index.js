//Getting HTML elements
const select = document.getElementById('laptops');
const specificLaptop = document.getElementById('specificLaptop');
const features = document.getElementById('features');
const workBtn = document.getElementById('workBtn');
const pay = document.getElementById('pay');
const bankBtn = document.getElementById('bankBtn');
const bankBalance = document.getElementById('bankBalance');
const getLoan = document.getElementById('getLoan');
const repayLoan = document.getElementById('repayLoan');
const repayBtn = document.getElementById('repayBtn');
const loanHTML = document.getElementById('loan');
const buyNow = document.getElementById('buyNow');
const success = document.getElementById('success');
const danger = document.getElementById('danger');
const boughtComputer = document.getElementById('boughtComputer');
//global variables
let bank = 0;
let work = 0;
let loanedMoney = 0;
let url = 'https://noroff-komputer-store-api.herokuapp.com/';
let computers;
let debt = false;
let pictureFix = '';

//This function fetches all the computers from the REST api. and fills the
const getComputers = async () => {
  console.log('Fetching API data');
  try {
    const response = await fetch(`${url}computers`);
    computers = await response.json();
    //this fixes the bug where the picture is not showing on computer "the visor". i replace .jpg with the correct file format. .png
    pictureFix = computers[4].image.replace('.jpg', '.png');
    console.log('Fetch was successful');
  } catch (error) {
    console.error('Something went wrong' + error);
  }
  fillDropdown(computers);
  changeComputer(computers);
};
//This function "fills" the dropdown menu with the different computers.
const fillDropdown = (data) => {
  for (let i = 0; i < data.length; i++) {
    select.innerHTML += `
        <option id="testing" value="${data[i].title}">${data[i].title}</option>
        `;
  }
};
//This function gets called whenever dropdown menu changes. it renders the computer info area.
const changeComputer = () => {
  let value = select.value;
  let specificComp;
  for (let i = 0; i < computers.length; i++) {
    if (value === computers[i].title) {
      specificComp = computers[i];
    }
  }
  specificLaptop.innerHTML = `
    <div class="specificLaptop p-4 d-flex align-items-center">
        <div >
            <img  height="100px" alt="Picture is missing"  src="${url}${
    specificComp.id === 5 ? pictureFix : specificComp.image
  }" />
        </div>
        <div>
            <h1>${specificComp.title}</h1>
            <p>${specificComp.description}</p>
        </div>
        <div>
            <h2>${specificComp.price} SEK</h2>
            <button onClick="buyComputer(${
              specificComp.price
            })" id="buyNow" class="btn btn-primary">buy now</button>
        </div>
    </div>
    `;
  features.innerHTML = `
        <p>${specificComp.specs}<p>
    `;
};
//This function triggers when pressing the "get a loan" button. The first check is to see if we already have a loan, and after that promps the user for an amount.
const loan = () => {
  if (!debt) {
    let loanAmount = prompt('How much do you want to loan?');
    if (loanAmount != '') {
      let balance = parseInt(bankBalance.innerHTML);
      if (loanAmount > balance * 2) {
        alert("can't get a loan double then your bank balance");
      }
      if (loanAmount <= balance * 2 && loanAmount != null) {
        debt = true;
        let oldBalance = parseInt(bankBalance.innerHTML);
        let parsedAmount = parseInt(loanAmount);
        loanedMoney = loanAmount;
        bankBalance.innerHTML = parsedAmount + oldBalance;
      }
    } else {
      alert('Need to enter a sum');
    }
  } else {
    alert('you already have a loan');
  }
  //Updating the outstanding loan on button.
  renderLoanButton(loanedMoney);
};

//this function renders the "repay loan" button first check is to see if we have a loan. then i remove the class hidden, to show the button. the parameter "loan" is the amount of money we have loaned, wich i also present on the button.
const renderLoanButton = (loan) => {
  if (!debt) return;
  repayBtn.classList.remove('hidden');
  loanHTML.classList.remove('hidden');
  loanHTML.innerHTML = loanedMoney;
  repayBtn.innerHTML = `
        <p>Repay loan <span>${loan}</span> :-</p>
    `;
};

//this is the button that that triggers on "repay loan" button. i have seperated the function into two different if stements, if current salary is higher then the loan, or if the salary is lower then the loan.
const repayLoanBtn = () => {
  let loanedMoneyInt = parseInt(loanedMoney);
  let workInt = parseInt(work);
  let balanceAfterPayment = loanedMoneyInt - workInt;
  let salaryLeft = workInt - loanedMoneyInt;

  if (workInt >= loanedMoneyInt) {
    let oldBalance = parseInt(bankBalance.innerHTML);
    renderLoanButton(balanceAfterPayment);
    loanedMoneyInt = balanceAfterPayment - workInt;
    work = salaryLeft;
    bankBalance.innerHTML = work + oldBalance;
    pay.innerHTML = 0;
    work = 0;
  } else if (workInt < loanedMoneyInt) {
    work = 0;
    pay.innerHTML = work;
    loanedMoney = loanedMoneyInt - workInt;
    renderLoanButton(balanceAfterPayment);
  } else {
    alert('not enough funds to repay loan');
  }

  if (balanceAfterPayment <= 0) {
    repayBtn.classList.add('hidden');
    loanHTML.classList.add('hidden');
    debt = false;
  }
};
//this increments the salary by 100 on press
const getToWork = () => {
  work += 100;
  pay.innerHTML = work;
};
//triggers when pressing the "bank" button, sends money to the bank, and if we have a loan, we will do the correct math to make sure the bank gets their interest.
const sendToBank = () => {
  if (debt) {
    let oldBalance = parseInt(bankBalance.innerHTML);
    let interest = work * 0.1;
    let salaryWithLoan = work * 0.9;
    loanedMoney = parseInt(loanedMoney) + interest;
    bankBalance.innerHTML = oldBalance + salaryWithLoan;
    pay.innerHTML = 0;
    work = 0;
    renderLoanButton(loanedMoney);
  } else {
    let oldBalance = parseInt(bankBalance.innerHTML);
    bank = work;
    work = 0;
    pay.innerHTML = 0;
    bankBalance.innerHTML = bank + oldBalance;
  }
};
//this functions triggers on the "buy now" button. checks if we kan afford a computer, if so deduct the amount and show a message.
const buyComputer = (price) => {
  let balance = parseInt(bankBalance.innerHTML);
  if (price <= balance) {
    bankBalance.innerHTML = balance - price;
    success.classList.remove('hidden');
    boughtComputer.innerHTML = `
                <span>Computer was bought for ${price} :-</span>
            `;
    setTimeout(() => {
      success.classList.add('hidden');
    }, 1500);
  } else {
    danger.classList.remove('hidden');
    setTimeout(() => {
      danger.classList.add('hidden');
    }, 2000);
  }
};
//Eventlistener
repayBtn.addEventListener('click', repayLoanBtn);
getLoan.addEventListener('click', loan);
bankBtn.addEventListener('click', sendToBank);
workBtn.addEventListener('click', getToWork);
select.addEventListener('change', changeComputer);
getComputers();
